// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_20200615CharHealthComponent.h"


void UTDS_20200615CharHealthComponent::ChangetHealthValue(float ChangeValue)
{
	//Super::ChangetHealthValue(ChangeValue);

	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && CurrentDamage < 0.0f)
	{
		if (Shield < 0.0f)
		{
			// add FX
			UE_LOG(LogTemp, Warning, TEXT("UTDS_20200615CharHealthComponent::ChangetHealthValue - Shield < 0.0f"));
		}
	}
	else
	{
		Super::ChangetHealthValue(ChangeValue);
	}

	if (ChangeValue < 0.0f)
	{
		ChangeShieldValue(CurrentDamage);
	}
}

float UTDS_20200615CharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDS_20200615CharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}

	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTDS_20200615CharHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDS_20200615CharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDS_20200615CharHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTDS_20200615CharHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

void UTDS_20200615CharHealthComponent::ReSetShieldRecoveryRateTimer()
{

}
