// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_20200615HealthComponent.h"

// Sets default values for this component's properties
UTDS_20200615HealthComponent::UTDS_20200615HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_20200615HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDS_20200615HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDS_20200615HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_20200615HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTDS_20200615HealthComponent::ChangetHealthValue(float ChangeValue)
{
	ChangeValue *= CoefDamage;

	Health += ChangeValue;

	if (Health > 100.f)
	{
		Health = 100.f;
	}
	else
	{
		if (Health <= 0.0f)
		{
			OnDead.Broadcast();
		}
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}
