// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_20200615Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"

#include "Materials/Material.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "Kismet/KismetStringLibrary.h"

#include "Engine/World.h"

ATDS_20200615Character::ATDS_20200615Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	
	/*
	// Create a decal in the world to show the cursor's location
	
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	*/

	InventoryComponent = CreateDefaultSubobject<UTDS_20200615InventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_20200615Character::InitWeapon);
	}

	HealthComponent = CreateDefaultSubobject<UTDS_20200615CharHealthComponent>(TEXT("HealthComponent"));

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATDS_20200615Character::CharDead);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDS_20200615Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	
	/*
	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	*/

	if (CurrentCursor)
	{
		APlayerController* muPC = Cast<APlayerController>(GetController());
		if (muPC)
		{
			FHitResult TraceHitResult;
			muPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDS_20200615Character::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDS_20200615Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_20200615Character::InputAxisForvard);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_20200615Character::InputAxisRight);

	PlayerInputComponent->BindAction(TEXT("FireEvent"),EInputEvent::IE_Pressed ,this , &ATDS_20200615Character::InitAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireEvent"),EInputEvent::IE_Released , this, & ATDS_20200615Character::InitAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_20200615Character::TryReloadWeapon);

	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_20200615Character::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDS_20200615Character::TrySwitchPreviosWeapon);

	PlayerInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDS_20200615Character::TryAbilityEnabled);
}

void ATDS_20200615Character::InputAxisForvard(float Value)
{
	AxisForvard = Value;
}

void ATDS_20200615Character::InputAxisRight(float Value)
{
	AxisRight = Value;
}

void ATDS_20200615Character::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ATDS_20200615Character::TrySwitchNextWeapon()
{
	/*if (InventoryComponent->WeaponSlots.Num() > 1)
	{

	}*/
	int8 OldIndex = CurrentIndexWeapon;
	FAdditionalWeaponInfo OldInfo;
	if (CurrentWeapon)
	{
		OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		if (CurrentWeapon->WeaponReloading)
		{
			CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwichWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{

			}
		}
	}
}

void ATDS_20200615Character::TrySwitchPreviosWeapon()
{
	int8 OldIndex = CurrentIndexWeapon;
	FAdditionalWeaponInfo OldInfo;
	if (CurrentWeapon)
	{
		OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		if (CurrentWeapon->WeaponReloading)
		{
			CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwichWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}

void ATDS_20200615Character::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

void ATDS_20200615Character::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisForvard);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisRight);

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, true, ResultHit);
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}
		}

		//Sprint
		if (SprintButtonFlag)
		{
			SprintAndGazeMatchingFunction();
		}
	}
	
	//if (MovementState == EMovementState::SprintRun_State)
	//{
	//	FVector myRotationVector = FVector(AxisForvard, AxisRight, 0.f);
	//	FRotator myRotator = myRotationVector.ToOrientationRotator();
	//	SetActorRotation((FQuat(myRotator)));
	//}
	//else
	//{

	//}
	//

	/*
	if (CurrentWeapon) 
	{
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
		{
			CurrentWeapon->ShouldReduceDispersion = true;
		}
		else
		{
			CurrentWeapon->ShouldReduceDispersion = false;
		}
	}
	*/
}

void ATDS_20200615Character::CaracterUpdate()
{
	float ResultSpeed = MovementInfo.RunSpeed;
	switch (MovementState)
	{
	case EMovementState::AimWalk_State:
		ResultSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResultSpeed = MovementInfo.SprintRunSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDS_20200615Character::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	AimFlag = false;
	WalkFlag = false;
	SprintRunFlag = false;
	CaracterUpdate();
}

void ATDS_20200615Character::ChangeMovementStateByFlags(/*bool NewAimFlag, bool NewWalkFlag, bool NewSprintRunFlag*/)
{
	//AimFlag = NewAimFlag;
	//WalkFlag = NewWalkFlag;
	//SprintRunFlag = NewSprintRunFlag;

	if (!AimFlag && !WalkFlag && !SprintRunFlag)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunFlag)
		{
			WalkFlag = false;
			AimFlag = false;
			MovementState = EMovementState::SprintRun_State;
		}
		else
		{
			if (AimFlag && WalkFlag)
			{
				MovementState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkFlag)
				{
					MovementState = EMovementState::Walk_State;
				}
				else
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CaracterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDS_20200615Character::SprintAndGazeMatchingFunction()
{
	FVector VectorForvard = GetCapsuleComponent()->GetForwardVector();
	//FVector VectorForvard = GetActorForwardVector();
	FVector VectorVelocity = GetVelocity();
	VectorVelocity.Normalize();
	bool FlagTest = VectorForvard.Equals(VectorVelocity, SprintVariance);
	/*
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(1, 0.0f, FColor::Green, UKismetStringLibrary::Conv_BoolToString(FlagTest));
		GEngine->AddOnScreenDebugMessage(2, 0.0f, FColor::Blue, UKismetStringLibrary::Conv_VectorToString(VectorVelocity));
	}
	*/
	if (SprintRunFlag != FlagTest)
	{
		SprintRunFlag = FlagTest;
		ChangeMovementStateByFlags();
	}
}

UDecalComponent* ATDS_20200615Character::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDS_20200615Character::InitAttackPressed()
{
	AttackCharEvent(true);
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, FString::Printf(TEXT("TRUE")));
}

void ATDS_20200615Character::InitAttackReleased()
{
	AttackCharEvent(false);
	//GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Green, FString::Printf(TEXT("FALSE")));
}

void ATDS_20200615Character::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = GetCurrentWeapon();

	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_20200615Character::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

void ATDS_20200615Character::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}


	UTDS_20200615GameInstance* myGI = Cast<UTDS_20200615GameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParameters;
				SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParameters.Owner = this;
				SpawnParameters.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParameters));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("hand_r_WeaponSocket"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;

					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound; // add reload round
					myWeapon->SReloadTime = myWeaponInfo.ReloadTime;	// HUD debug, remove!!!

					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					
					//if(InventoryComponent)
					//{
						CurrentIndexWeapon = NewCurrentIndexWeapon; /*InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName)*/
					//}

					//delegate
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_20200615Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_20200615Character::WeaponReloadEnd);
					myWeapon->OnWeaponFireAnim.AddDynamic(this, &ATDS_20200615Character::WeaponFireAnim);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if(InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDS_20200615Character::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

AWeaponDefault* ATDS_20200615Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_20200615Character::WeaponFireAnim(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponFireAnim_BP(Anim);
}

void ATDS_20200615Character::WeaponFireAnim_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDS_20200615Character::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_20200615Character::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

float ATDS_20200615Character::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		HealthComponent->ChangetHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UMoveTypes::AddEffectBySurfaceTupe(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}
	
	return ActualDamage;
}

void ATDS_20200615Character::CharDead()
{
	int32 rand = FMath::RandHelper(DeadAnimMontage.Num());
	float DeadAnimTimer = 0.f;
	if (DeadAnimMontage.IsValidIndex(rand) && DeadAnimMontage[rand] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		DeadAnimTimer = DeadAnimMontage[rand]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadAnimMontage[rand]);
	}

	UnPossessed();
	bIsAlive = false;

	GetCursorToWorld()->SetVisibility(false);

	//Timer regdol
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_20200615Character::EnableRagdoll, DeadAnimTimer, false, -1.f);
}

void ATDS_20200615Character::EnableRagdoll()
{
	//UE_LOG(LogTemp, Warning, TEXT("ATDS_20200615Character::EnableRagdoll - It's work"));

	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ATDS_20200615Character::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDS_20200615Character::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	//in BP
}


EPhysicalSurface ATDS_20200615Character::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}

	return Result;
}

TArray<UTDS_StateEffect*> ATDS_20200615Character::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_20200615Character::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);
}

void ATDS_20200615Character::AddEffect(UTDS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

FName ATDS_20200615Character::AttachEffectToActor(UTDS_StateEffect* NewEffect, FVector& Offset)
{
	FName NameBoneToAttached = TEXT("rootSocket");

	return NameBoneToAttached;
}
