// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "../FunctionLibrary/MoveTypes.h"
#include "../Items/Weapon/WeaponDefault.h"
#include "../Game/TDS_20200615GameInstance.h"
#include "../ActorComponent/TDS_20200615InventoryComponent.h"
#include "../ActorComponent/TDS_20200615CharHealthComponent.h"
#include "../Interface/TDS_IGameActor.h"

#include "../StateEffects/TDS_StateEffect.h"

#include "TDS_20200615Character.generated.h"

UCLASS(Blueprintable)
class ATDS_20200615Character : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATDS_20200615Character();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_20200615InventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_20200615CharHealthComponent* HealthComponent;

public:
	/**/
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/*Imputs*/
	UFUNCTION()
		void InputAxisForvard(float Value);
	float AxisForvard = 0.0f;
	UFUNCTION()
		void InputAxisRight(float Value);
	float AxisRight = 0.0f;
	UFUNCTION()
		void InitAttackPressed();
	UFUNCTION()
		void InitAttackReleased();
	UFUNCTION()
		void TryReloadWeapon();
	UFUNCTION()
		void TrySwitchNextWeapon();
	UFUNCTION()
		void TrySwitchPreviosWeapon();
	UFUNCTION()
		void TryAbilityEnabled();
	

	/*Tick Func*/
	UFUNCTION()
		void MovementTick(float DeltaTime);

	/*Func*/
	UFUNCTION(BlueprintCallable)
		void CaracterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
		void ChangeMovementStateByFlags(/*bool NewAimFlag, bool NewWalkFlag, bool NewSprintRunFlag*/);

	/*Movement*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCaracterSpeed MovementInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimFlag = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkFlag = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunFlag = false;

	/*MySprint*/
	UFUNCTION()
		void SprintAndGazeMatchingFunction();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintButtonFlag = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintVariance = 0.7f;

	/*Cursor*/
	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInstance* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.f, 40.f, 40.f);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	/*Weapon*/
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FName InitWeaponName = FName(TEXT("Rifle_v1"));
	AWeaponDefault* CurrentWeapon = nullptr;
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION()
		void WeaponFireAnim(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireAnim_BP(UAnimMontage* Anim);
	
	/*Reload*/
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	/*IndexWeapon*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	/*Damage*/
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(BlueprintCallable)
	void CharDead();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Live")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Live")
		TArray<UAnimMontage*> DeadAnimMontage;
	FTimerHandle TimerHandle_RagDollTimer;
	void EnableRagdoll();

	/*Interfase*/
	EPhysicalSurface GetSurfaceType() override;

	/*Effects*/
	TArray<UTDS_StateEffect*> Effects;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_StateEffect* RemovedEffect) override;
	void AddEffect(UTDS_StateEffect* NewEffect) override;
	FName AttachEffectToActor(UTDS_StateEffect* NewEffect, FVector& Offset) override;

	/*Ability*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDS_StateEffect> AbilityEffect = nullptr;

};