// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "../FunctionLibrary/MoveTypes.h"
#include "../Items/Weapon/WeaponDefault.h"
#include "TDS_20200615GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_20200615_API UTDS_20200615GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting")
		UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
};
