// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_20200615GameMode.h"
#include "TDS_20200615PlayerController.h"
#include "../Character/TDS_20200615Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_20200615GameMode::ATDS_20200615GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_20200615PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TDS/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ATDS_20200615GameMode::PlayerCharacterDead()
{
}
