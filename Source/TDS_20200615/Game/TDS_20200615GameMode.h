// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_20200615GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_20200615GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_20200615GameMode();

	void PlayerCharacterDead();
};



