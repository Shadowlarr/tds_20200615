// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../StateEffects/TDS_StateEffect.h"

#include "TDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_20200615_API ITDS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	// For Test
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
		void AvailableForEffectsBP();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
		bool AvailableForEffects();
	//

	virtual bool AvailableForEffectsOnlyCPP();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_StateEffect* RemovedEffect);
	virtual void AddEffect(UTDS_StateEffect* NewEffect);

	//FName SocetToAttachEffect = NAME_None; //rootSocket
	//FVector LocationToAttachEffect = FVector(100);

	virtual FName AttachEffectToActor(UTDS_StateEffect* NewEffect, FVector& Offset);

};
