// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnvironmentStructure.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATDS_EnvironmentStructure::ATDS_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ATDS_EnvironmentStructure::AvailableForEffects_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ATDS_EnvironmentStructure::AvailableForEffects_Implementation"));
	return true;
}

bool ATDS_EnvironmentStructure::AvailableForEffectsOnlyCPP()
{
	UE_LOG(LogTemp, Warning, TEXT("ATDS_EnvironmentStructure::AvailableForEffectsOnlyCPP"));
	return true;
}

EPhysicalSurface ATDS_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDS_StateEffect*> ATDS_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_EnvironmentStructure::RemoveEffect(UTDS_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);
}

void ATDS_EnvironmentStructure::AddEffect(UTDS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

FName ATDS_EnvironmentStructure::AttachEffectToActor(UTDS_StateEffect* NewEffect, FVector& Offset)
{
	Offset = FVector(0,0,250);
	return NAME_None;
}
