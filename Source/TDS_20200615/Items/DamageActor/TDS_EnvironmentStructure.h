// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Interface/TDS_IGameActor.h"
#include "../../StateEffects/TDS_StateEffect.h"

#include "TDS_EnvironmentStructure.generated.h"

UCLASS()
class TDS_20200615_API ATDS_EnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// For Test
	bool AvailableForEffects_Implementation() override;
	//

	bool AvailableForEffectsOnlyCPP() override;

	EPhysicalSurface GetSurfaceType() override;

	/*Effects*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		TArray<UTDS_StateEffect*> Effects;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_StateEffect* RemovedEffect) override;
	void AddEffect(UTDS_StateEffect* NewEffect) override;
	FName AttachEffectToActor(UTDS_StateEffect* NewEffect, FVector& Offset) override;
};
