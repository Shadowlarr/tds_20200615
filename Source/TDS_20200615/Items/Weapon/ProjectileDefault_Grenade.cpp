// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExploreShow = 0;
FAutoConsoleVariableRef CVARExploreShow(TEXT("Grenate.DebugExploreShow"), DebugExploreShow, TEXT("Drow debug for Grenate"), ECVF_Cheat);

// Called when the game starts or when spawned
void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComponent, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	TimeToExplose = ProjectileSetting.ExploseTime;
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.f));

	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	// ���� �� ������
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSetting.ExploseMinRadiusDamage,
		ProjectileSetting.ExploseMaxRadiusDamage,
		ProjectileSetting.ExploseDamageFalloff,
		NULL, IgnoredActor, this, nullptr, ECC_Visibility
	);

	if (DebugExploreShow != 0)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ExploseMaxRadiusDamage, 32, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ExploseMinRadiusDamage, 32, FColor::Blue, false, 4.0f);
	}

	this->Destroy();
}
