// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponDefault.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetMathLibrary.h"

#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"

#include "../../FunctionLibrary/MoveTypes.h"
#include "../../ActorComponent/TDS_20200615InventoryComponent.h"
#include "../../StateEffects/TDS_StateEffect.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDropTick(DeltaTime);
	ClipDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	//GEngine->AddOnScreenDebugMessage(2, 0.0f, FColor::Blue, UKismetStringLibrary::Conv_FloatToString(FireTimer));
	/*if (GetWeaponRound() > 0)
	{*/
		if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
		{
			if (FireTimer < 0.0f)
			{
				if (!WeaponReloading)
				{
					Fire();
				}
			}
			else
			{
				FireTimer -= DeltaTime;
			}
		}
		else
		{
			if (FireTimer >= 0.0f)
			{
				FireTimer -= DeltaTime;
			}
		}
	/*}
	else
	{
		if (!WeaponReloading && CheckCanWeaponReload())
		{
			InitReload();
		}
	}*/
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	//GEngine->AddOnScreenDebugMessage(1, 0.0f, FColor::Red, UKismetStringLibrary::Conv_FloatToString(ReloadTimer));
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else 
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}

	if (ShowDebug)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f."), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}
}

void AWeaponDefault::Fire()
{
	//fire logic
	AdditionalWeaponInfo.Round -= 1;
	FireTimer = WeaponSetting.RateOfFire;
	ChangeDispersionBuShot();
	int8 NumberProjectileByShot = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotarion = ShootLocation->GetComponentRotation();

		ProjectileInfo = GetProjectileInfo();
		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectileByShot; ++i)
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
				FVector Dir = /*ShootEndLocation*/ EndLocation - SpawnLocation;
				Dir.Normalize();
				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotarion = myMatrix.Rotator();

				FActorSpawnParameters SpawnParametres;
				SpawnParametres.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParametres.Owner = GetOwner();
				SpawnParametres.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotarion, SpawnParametres));

				if (myProjectile)
				{
					//myProjectile->BulletProjectileMovement->InitialSpeed = ProjectileInfo.ProjectileInitSpeed;
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				//line trase
				FireTrace(SpawnLocation, EndLocation);
			}
		}
	}

	//start fire animation
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	}
	else
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFire;
	}
	OnWeaponFireAnim.Broadcast(AnimToPlay);

	//weapon fire anim if Skeletal mesh thue
	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}

	// drop shell
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.f)
		{
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulsDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpuls, WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	//sound
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());

	//emiter
	if (WeaponSetting.EffectFireWeaponCorrectRotation == FRotator(0.f, 0.f, 0.f))
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());
	}
	else
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, FTransform(WeaponSetting.EffectFireWeaponCorrectRotation, ShootLocation->GetComponentLocation(), ShootLocation->GetComponentScale()));
	}

	//reload
	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
		{
			InitReload();
		}
	}
}

void AWeaponDefault::FireTrace(FVector& SpawnLocation, FVector& EndLocation)
{
	FHitResult Hit;
	TArray<AActor*> Actors;

	//float DebugLineTimet = 0.0f;
	if (ShowDebug)
	{
		//DebugLineTimet = 5.f;
		DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	FVector EndDistLocation = FVector(0.f);
	//EndDistLocation = EndLocation * WeaponSetting.DistanceTrace; // not work!!!
	EndDistLocation = SpawnLocation + EndLocation.GetClampedToMaxSize(WeaponSetting.DistanceTrace);

	UKismetSystemLibrary::LineTraceSingle(
		GetWorld(), 
		SpawnLocation, 
		EndDistLocation,
		ETraceTypeQuery::TraceTypeQuery4, 
		false,
		Actors,
		EDrawDebugTrace::ForDuration, 
		Hit, 
		true, 
		FLinearColor::Red,
		FLinearColor::Green,
		5.f /*DebugLineTimet*/
		);

	if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurface = UGameplayStatics::GetSurfaceType(Hit);

		if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurface))
		{
			UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurface];
			if (myMaterial && Hit.GetComponent())
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.f);
			}
		}

		if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurface))
		{
			UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurface];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.f)));
			}
		}

		if (WeaponSetting.ProjectileSetting.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
		}


		UMoveTypes::AddEffectBySurfaceTupe(Hit.GetActor(), ProjectileInfo.Effect, mySurface);

		//UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(Hit.GetActor(), FName("Effect"));

		//ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(Hit.GetActor());
		//if (myInterface)
		//{
			//myInterface->AvailableForEffectsOnlyCPP();
			
			//EPhysicalSurface mySurface;
			//mySurface = myInterface->GetSurfaceTupe();	
		//}

		// For Test
		//if (Hit.GetActor()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
		//{
		//	ITDS_IGameActor::Execute_AvailableForEffects(Hit.GetActor());
		//	ITDS_IGameActor::Execute_AvailableForEffectsBP(Hit.GetActor());
		//}
		//

		UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);

		//UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.WeaponDamage, GetInstigatorController(), this, NULL);
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectileInfo()
{
	return WeaponSetting.ProjectileSetting;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	//start character reload animation
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReloadAim;
	}
	else
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;
	}
	OnWeaponReloadStart.Broadcast(AnimToPlay);

	// start weapon reload animation
	UAnimMontage* AnimWeaponToPlay = nullptr;
	if (WeaponAiming)
	{
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
	}
	else
	{
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
	}

	//weapon Reload anim if Skeletal mesh thue
	if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
	}

	// drop Clip
	if (WeaponSetting.ClipDropMesh.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedTakeFromInveory = 0;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AvailableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round = AvailableAmmoFromInventory;
		AmmoNeedTakeFromInveory = AvailableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInveory = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInveory);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;

	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	
	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTDS_20200615InventoryComponent* myInventory = Cast<UTDS_20200615InventoryComponent>(GetOwner()->GetComponentByClass(UTDS_20200615InventoryComponent::StaticClass()));
		if (myInventory)
		{
			int8 AvailableAmmoForWeapon = 0;
			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 result = WeaponSetting.MaxRound;
	int8 AvailableAmmoForWeapon = 0;
	if (GetOwner())
	{
		UTDS_20200615InventoryComponent* myInventory = Cast<UTDS_20200615InventoryComponent>(GetOwner()->GetComponentByClass(UTDS_20200615InventoryComponent::StaticClass()));
		if (myInventory)
		{
			if (myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				result = AvailableAmmoForWeapon;
			}
		}
	}
	return result;
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimReduction;
		WeaponAiming = true;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimReduction;
		WeaponAiming = true;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimReduction;
		WeaponAiming = false;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionAimReduction;
		WeaponAiming = false;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		WeaponAiming = false;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
	ChangeDispersionBuShot();
}

void AWeaponDefault::ChangeDispersionBuShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction progectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
	return EndLocation;
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulsDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpuls, WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset, WeaponSetting.ClipDropMesh.DropMeshImpulsDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime, WeaponSetting.ClipDropMesh.ImpulseRandomDispersion, WeaponSetting.ClipDropMesh.PowerImpuls, WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDirection, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		FVector LocalDir = (this->GetActorForwardVector() * Offset.GetLocation().X) + (this->GetActorRightVector() * Offset.GetLocation().Y) + (this->GetActorUpVector() * Offset.GetLocation().Z); //not rotate!!!
		//GEngine->AddOnScreenDebugMessage(1, 0.2f, FColor::Green, LocalDir.ToString());
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		
		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
				
		AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			//GEngine->AddOnScreenDebugMessage(2, 0.2f, FColor::Green, FString::Printf(TEXT("Good")));
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			NewActor->SetActorTickEnabled(false);

			NewActor->SetLifeSpan(LifeTimeMesh);
			//NewActor->InitialLifeSpan = LifeTimeMesh; //not work

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel11, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel12, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass);
			}

			if (!DropImpulseDirection.IsNearlyZero())
			{
				//GEngine->AddOnScreenDebugMessage(3, 0.2f, FColor::Green, FString::Printf(TEXT("Impylce")));
				FVector FinalDir;
				LocalDir += (DropImpulseDirection * 1000.f);

				if (!FMath::IsNearlyZero(ImpulseRandomDirection))
				{
					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDirection);
				}
				FinalDir.GetSafeNormal(0.0001f);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			}
		}
	}
}
