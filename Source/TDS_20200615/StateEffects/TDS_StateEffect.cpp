// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "../ActorComponent/TDS_20200615HealthComponent.h"
#include "../Interface/TDS_IGameActor.h"
#include "../Character/TDS_20200615Character.h"

#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	//UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect::InitObject "), AcmyActortor);
	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDS_20200615HealthComponent* myHealthComponent = Cast<UTDS_20200615HealthComponent>(myActor->GetComponentByClass(UTDS_20200615HealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->ChangetHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
		if (myInterface)
		{
			FVector Location = FVector(0);
			FName NameBoneToAttached = NAME_None;
				
			NameBoneToAttached = myInterface->AttachEffectToActor(this, Location);

			if (NameBoneToAttached != NAME_None)
			{
				ACharacter* myChar = Cast<ACharacter>(myActor);
				if (myChar)
				{
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myChar->GetMesh(), NameBoneToAttached);
				}
			}
			else
			{
				ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NAME_None, Location, FRotator::ZeroRotator);
			}

			//FString myString = NameBoneToAttached.ToString();
			//UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_ExecuteTimer::NameBoneToAttached %s"), *myString);
			//myString = Location.ToString();
			//UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_ExecuteTimer::Location %s"), *myString);
		}
	}

	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter != nullptr)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}

	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDS_20200615HealthComponent* myHealthComponent = Cast<UTDS_20200615HealthComponent>(myActor->GetComponentByClass(UTDS_20200615HealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->ChangetHealthValue(Power);
		}
	}
}
