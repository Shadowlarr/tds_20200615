// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_20200615 : ModuleRules
{
	public TDS_20200615(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore" });
    }
}
