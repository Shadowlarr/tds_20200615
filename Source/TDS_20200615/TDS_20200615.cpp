// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_20200615.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_20200615, "TDS_20200615" );

DEFINE_LOG_CATEGORY(LogTDS_20200615)
 